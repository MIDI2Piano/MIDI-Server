### MIDI Server

A quick-and-dirty express based server for uploading MIDI files and converting them to SVG and PNG.
Runs on port 9801 by default. Edit server.js to change. (No config system at the moment)

*REQUIREMENTS:*
- Linux based system
- MuseScore installed
- InkScape installed

*Endpoints:*
- POST /midi (FormData: 'midi' = MIDI file) - Uploads a midi file to the server and creates PNGs and SVGs from it.
- GET /:format/:filename - Retrieves :filename in :format where format is 'midi', 'png', or 'svg'.

This is very basic at the moment.
