// COPYRIGHT (c) 2016 MIDI 2 Piano

'use strict';

// v1/api.js - API Version 1 Main file.

const express = require('express');
const multer = require('multer');
const router = express.Router();
const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const spawn = require('child_process').spawnSync;
const Logger = require('../../util/logger');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'data/midi/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})

const uploader = multer({
  storage: storage,
  dest: 'data/midi/',
});

// Post file data in the formdata 'midi' to endpoint /midi/FILETITLE
router.post('/midi', uploader.single('midi'), function(req, res) {
  const basename = path.basename(req.file.originalname, path.extname(req.file.originalname))

  // Create SVG from MIDI (MuseScore)
  const museScoreSVG = spawn('musescore', [`./data/midi/${req.file.originalname}`, '-o', `./data/svg/${basename}.svg`])
  // Create PDF from MIDI (MuseScore)
  const museScorePDF = spawn('musescore', [`./data/midi/${req.file.originalname}`, '-o', `./data/pdf/${basename}.pdf`])
  // Convert to lower-res PNG (Inkscape)
  const inkScape = spawn('inkscape', ['-z', '-e', `./data/png/${basename}.png`, '-h', '1920', `./data/svg/${basename}.svg`])

  res.sendStatus(200)
});

// Get file data from /FORMAT/FILETITLE where format is midi, png, or svg
router.get('/:format/:file', function(req, res) {
  const formats = ['midi', 'svg', 'pdf', 'png']

  if(formats.indexOf(req.params.format) === -1) {
    res.sendStatus(404)
    return
  }

  try {
    const filePath = path.join(__dirname, `../../data/${req.params.format}/${req.params.file}`)
    // Stat to emit error if file doesn't exist.
    fs.statSync(filePath)
    res.sendFile(filePath)
    return
  } catch (e) {
    res.sendStatus(404)
    return
  }
})

// Make write dirs to ensure they exist
mkdirp.sync(path.join(__dirname, `../../data/midi`))
mkdirp.sync(path.join(__dirname, `../../data/png`))
mkdirp.sync(path.join(__dirname, `../../data/pdf`))
mkdirp.sync(path.join(__dirname, `../../data/svg`))

module.exports = router;
