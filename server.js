#!/usr/bin/env node
// COPYRIGHT (c) 2015 Joshua Bemenderfer
'use strict';

// Load dependencies.
let fs = require('fs');
let path = require('path');
let https = require('https');
let express = require('express');
let helmet = require('helmet');
let Logger = require('./util/logger');

let API = require('./api/v1/api.js');

// Set up global client & server configuration.
let Instance = express();

// Basic Security Measures.
Instance.use(helmet.hidePoweredBy());
Instance.use(helmet.ieNoOpen());
Instance.use(helmet.hsts());
Instance.use(helmet.noSniff());
Instance.use(helmet.xssFilter());

// Enable CORS.
Instance.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');

  // Handle OPTIONS requests
  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  } else {
    next();
  }
});

Instance.use(API);

let Server = Instance.listen(9801, function () {
  let port = Server.address().port;

  Logger.info('Server started on port %s', port);
});
